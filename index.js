db.fruits.insertMany([
  {
    name : "Apple",
    supplier: "Red Farms Inc.",
    stocks : 20,
    price: 40,
    onSale : true,
  },

  {
    name : "Banana",
    supplier : "Yellow Farms",
    stocks : 15,
    price: 20,
    onSale : true,
  },

  {
    name : "Kiwi",
    supplier : "Green Farming and Canning",
    stocks : 25,
    price: 50,
    onSale : true,
  },

  {
    name : "Mango",
    supplier : "Yellow Farms",
    stocks : 10,
    price: 60,
    onSale : true,
  },
  {
    name : "Dragon Fruit",
    supplier: "Red Farms Inc.",
    stocks : 10,
    price: 60,
    onSale : true,
  },

])


//Aggregation
// Aggregation in MongoDB is typically done 2-3 steps. Eacg process/step in aggregation is called a stage.

db.fruits.aggregate([
      // Aggregate documents to get the total stocks of fruits per supplier.

      // $match - used to match or get documents that satisfies the condition.
      // syntax: {match:{field:<value>}}
        {$match:{onSale:true}}, //apple,dragon fruit,banana,mango,kiwi

        // $group - allows us to group together documents and create an analysis out of the grouped documents

        // _id: in the group stage, essentially associates an id to our results.
        // _id: also determines the number of groups.

        // _id: "$supplier" - essentially groups together documents with the same values in the supplier field.

         // _id: "$<field>" - groups documents based on  the value of the indicated field.

        /*
          $match - apple, kiwi, banana, dragon fruit, mango

          $group - _id - $supplier

          $sum - used to add or total values in a given field

          $sum: "stocks" - we got the sum of the values of the stocks field of each item per group.

          $sum: "$<field>" - sums the values of the given field per group


          _id: Red Farms
            apple 
            supplier - Red Farms
            stocks: 20
            dragon fruit
            supplier - Red Farms
            stocks: 10

            $sum: 30




          _id: Green Farming
            kiwi
            supplier - Green Farming
            stocks: 25
            $sum: 25
           

          Supplier: Yellow Farms  
            banana
            supplier - Yellow Farms
            stockets:15
            mango
            supplier - Yellow Farms
            stocks:10

            $sum: 25

        */

        {$group:{_id:"$supplier",totalStocks:{$sum:"$stocks"}}}

])



db.fruits.aggregate([

        {$match:{onSale:true}},
        {$group:{_id:"$supplier",totalStocks:{$sum:"$stocks"},avgPrice:{$avg:"$price"}},
        
    ])


// What if we add the value of _id as null?
// If the _id's value is definite or given, $group will only create one group.
db.fruits.aggregate([

        {$match:{onSale:true}},
        {$group:{_id:null,totalStocks:{$sum:"$stocks"}}}
        
    ])

db.fruits.aggregate([

        {$match:{onSale:true}},
        {$group:{_id:"AllFruits",totalStocks:{$sum:"$stocks"}}}
        
    ])
db.fruits.aggregate([

        {$match:{onSale:true}},
        {$group:{_id:"OneGroup",totalStocks:{$sum:"$stocks"}}}
        
    ])




db.fruits.aggregate([
        // $match stage is similar to find(). In fact, you can even add query operators to expand the criteria.
        // Mango, Kiwi, Apple,
        {$match:{$and:[{onSale:true},{price:{$gte:40}}]}},
        {$group:{_id:"$supplier",totalStocks:{$sum:"$stocks"}}}
        
    ])

// $avg - is an operator used in $group stage
// $avg - hets the avg of the values of the given field per group.

// Gets the average stock of fruits pers supplier for all items on sale.

db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group:{_id:"$supplier",avgStocks:{$avg:"$stocks"}}}
        
    ])



db.fruits.aggregate([
        
        // Apple, Kiwi, Mango, Banana
        {$match:{onSale:true}},
        {$group:{_id:null,avgPrice:{$avg:"$price"}}}
        
    ])

// gets the average price of all fruits which supplier is Red Farms Inc.
db.fruits.aggregate([
        // Apple, Dragonfruit
        {$match:{supplier:"Red Farms Inc."}},
        {$group:{_id:"group1",avgPrice:{$avg:"$price"}}}
        
    ])

//$max - will allow us to get the highest value out of all the values in a given field per group.


// Gets the highest number of stock of all fruits on sale.
db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group:{_id:"maxStocckOnSale",maxStock:{$max:"$stocks"}}}
        
    ])


// Gets the highest price of fruits that are on sale.
db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group:{_id:"highestPrice",maxPrice:{$max:"$price"}}}
        
    ])



//$min - gets the lowest value of the given field per group.


// Gets the highest number of stock of all fruits on sale.
db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group:{_id:"lowestStockOnSale",minStock:{$min:"$stocks"}}}
        
    ])


db.fruits.aggregate([
        
        {$match:{supplier:"Yellow Farms"}},
        {$group:{_id:"lowestPriceYellowFarms",minPrice:{$min:"$price"}}}
        
    ])



// #count - is a stage usually added after $match to count all items that matches our criteria

// Count All Items on Sale
db.fruits.aggregate([
        
        // Banana, Apple, Kiwi, Mango
        {$match:{onSale:true}},
        {$count:"itemsOnSale"}
        
    ])

// Number of items which price are greater than 50
db.fruits.aggregate([
        
        {$match:{price:{$gt:50}}},
        {$count:"itemsPriceMoreThan50"}
        
    ])


db.fruits.aggregate([
        
        {$match:{$and:[{price:{$gte:50}},{stocks:{$lt:20}}]}},
        {$count:"itemsPriceMGreaterThan50Below20Stocks"}
        
    ])


// $out - save/outputs the results in a new collection.
// It is usually the last stage in an aggregation pipeline.
// Note: This will overwrite the collection if it already exists.

db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group:{_id:"$supplier",totalStocks:{$sum:"$stocks"}}},
        {$out: "stockPerSupplier"}
        
    ])


db.fruits.aggregate([
        
        {$match:{price:{$lte:50}}},
        {$group:{_id:"$supplier",totalStocks:{$sum:"$stocks"}}},
        {$out: "stockPerSupplier"}
        
 ])


db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group:{_id:"$supplier",avgPrice:{$avg:"$price"}}},
        {$out: "avgPricePerSupplier"}
        
 ])


// Mini Activity
// Use MongoDb aggregation to get the lowest number of stock per supplier for all items on sale.
// Then save the results in a collection called lowestNumberOfStock.
// Screenshot the resulting collection and send it in our hangouts


db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group:{_id:"$supplier",minStock:{$min:"$stocks"}}},
        {$out: "lowestNumberOfStock"}
        
 ])
